require 'yaml'
require 'active_support/all'

class Pfsrd
  def initiate
    loop do
      ('%s> ' % '$').display
      input = gets.chomp
      command, *params = input.split /\s/
      case command
      when /\Awat\z/i
        puts "wat"
      when /\Aexit\z/i
        exit
      else
        if respond_to?(command)
          send(command, *params)
        elsif !params.empty? && respond_to?(command + ' ' + params.first)
          c = command + ' ' + params.first
          p = params[1..-1]
          send(c, *p)
        else
          puts 'Invalid command'
        end
      end
    end
  end

  def campaign(dirname = 'characters')
    @characters ||= { }.with_indifferent_access
    Dir.entries(dirname).each do |character_file|
      next unless character_file.ends_with? '.yml'
      character = YAML::load(File.open(File.join('.', dirname, character_file)))
      character_name = character_file.gsub('.yml', '')
      @characters.merge!({ character_name => character})
    end
    make_skill_methods
  end

  def save(type = :fortitude, dc = 0, characters = @characters)
    each_character do |data|
      name = data[:name]
      roll = d20
      bonus = (data[:saves][type] || 0)
      puts "#{name} #{roll+ bonus} = #{roll} + #{bonus} (#{(roll + bonus) >= dc.to_i ? 'PASS' : 'FAIL'})"
    end
  end

  def characters
    each_character { |character| puts "#{name} :: #{data[:player]}" }
  end

  def help
    puts "no help"
  end

  private
  def make_skill_methods
    each_character do |data|
      next unless data[:skills]
      data[:skills].keys.each do |skill|
        selfclass.send(:define_method, skill) do |dc = 0|
          each_character do |d|
            next unless d[:skills]
            next unless d[:skills][skill]
            name = d[:name]
            roll = d20
            bonus = d[:skills][skill]
            puts "#{name} #{roll+ bonus} = #{roll} + #{bonus} (#{(roll + bonus) >= dc.to_i ? 'PASS' : 'FAIL'})"
          end
        end
      end
    end
  end

  def selfclass
    (class << self; self; end)
  end

  def each_character
    @characters.each_pair do |name, data|
      yield data
    end
  end

  [:fortitude, :reflex, :will].each do |type|
    define_method type do |*args|
      save(type, *args)
    end
  end

  [4, 6, 8, 12, 20].each do |die|
    define_method "d#{die}" do
      rand(die)+1
    end
  end
end


Pfsrd.new.initiate
